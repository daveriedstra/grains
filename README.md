# `grains~`

## Granular synthesis in Pure Data

_Version 0.1.1 - 2022-11-26_

### Dependencies

`grains~` was built in purr-data but the core can be used in pd vanilla without any external libraries. The help patch uses `cyclone` and the examples use `cyclone`, `iemlib`, and abstractions from [dried-utils](https://gitlab.com/daveriedstra/dried-utils).

### Usage

#### Arguments

**1: Polyphony** - How many grains can fire simultaneously. `grains~` uses `clone` behind the scenes to manage polyphony.

**2: Sample table name root** - This is the **root** of the sample tables `grains~` will read. `grains~` is stereo and therefore needs two channels, left and right. `grains~` assumes `-l` and `-r` suffixes for the left and right channels respectively. For example, if your table samples are `sample-l` and `sample-r`, you should provide `sample` for this argument.

#### Inlet

All `grains~` inlet values are per-grain, ie, set before a grain starts and not changed during that grain's playback. This means they can all be interpolated without needing to worry about pops or clicks.

#### Note on variation:

Many parameters are _variable_, meaning they can be randomized. All variable parameters have the following three additional controls:

* `parameter range` _(default: 0)_ - The range within which a random value might fall. This is centred on the assigned value; so if the assigned value is `2` and the range is `1`, values will be within `0.5` and `1.5`. This value uses the same units as the parameter it applies to.
* `parameter variation` _(0-1, default: 1)_- The likelihood that a new value will reach the extent of the range. Variation values closer to 0 mean that the new value is more likely to be close to the assigned value; variation values closer to `1` mean that the new value will be evenly distributed within the range.
* `parameter probability` _(0-1, default: 1)_ - The likelihood that the output value will be varied (as opposed to simply being the assigned value). For example, a probability value of `0.8` means that the output value will have 80% chance of being variably distributed according to `parameter range` and `parameter variation` and a 20% chance of being the value assigned by `parameter $1`.

**Basic control**

* `1`, `start` - start firing grains
* `0`, `stop` - stop firing grains
* `bang` - fire one grain
* `sample-rate $1` _(Hz)_ - sample rate of the grain sample buffer
* `sample-table $1` - set name for sample table
    * _Note: `grains~` only calculates the sample table length when the table is changed or when DSP is (re)started._
* `window-table $1` - set name for custom window table
    * _Note: `grains~` only calculates the window table length when the table is changed or when DSP is (re)started._
* `window-type $1` - sets the window type, overrides window-table. Accepted values: `hann` (default), `ramp-up`, `ramp-down`, `curve-up`, `curve-down`, `triangle`, `tukey`
* `tukey-sustain $1` _(0 - 1)_ - sets the tukey sustain proportion
* `circular-mode $1` _(1 / 0)_ - enable / disable circular mode (disabled by default). Default mode prevents grains from over- or under-reading the buffer (trying to access audio that doesn't exist), circular mode assumes the buffer is circular and reads around the edges.
    * It is still possible to cause glitches by telling `grains~` to read over the "record head" in the circular buffer. If using `dried-utils/circ-buff~`, this can be prevented by ensuring that the delay value sent to `circ-buff~` is at least `grain duration + position range + duration range + extra delay`.

**Parameter controls**

* `duration` _(ms, default: 50)_ - duration of each grain
    * **variable:** `duration range`, `duration variation`, `duration probability`
* `amplitude` _(dB, default: 0)_ - amplitude of each grain (-100 to 0 dB)
    * **variable:** `amplitude range`, `amplitude variation`, `amplitude probability`
    * _note: amplitude is in -100 - 0dB instead of 0 - 1 RMS so that both halves of the range have the same perceptual size._
* `interval` _(ms, default: 50)_ - interval between the starts of successive grains. Use `[expr pow($f1, -1) * 1000]` if you'd rather think in Hz.
    * **variable:** `interval range`, `interval variation`, `interval probability`
* `position` _(ms, default: 0)_ - position in the sample where the grains are taken from ("playhead" position). `grains~` does _not_ have built-in playhead controls, so you have to manage this yourself. To playback through the whole sample, run a `line` from 0 to the sample duration over the sample duration as the `start` value.
    * **variable:** `position range`, `position variation`, `position probability`
* `pan $1` _(-1 - 1)_ - pan position
    * **variable:** `pan range`, `pan variation`, `pan probability`
    * _note: it's possible to manipulate the stereo width of the source by sending_ `pan pre-width $1` _. This is useful for working with mono samples, ie, by sending 0._
* `rate $1` _(0 - 1 or more, default: 1)_ - playback rate of the sample
    * **variable:** `rate range`, `rate variation`, `rate probability`

#### Outlets

**First outlet:** Left channel

**Second outlet:** Right channel

**Third outlet:** Bang immediately before each grain
